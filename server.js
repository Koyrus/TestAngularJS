var express  = require('express');
var cors = require('cors');
var app = express();
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var db;
var path = require('path');
var ObjectID  = require('mongodb').ObjectID;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(cors());
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.header('Access-Control-Allow-Credentials', true);
    next();
});
var json  =[ { procedure: 'Procedure 1 (A) ', name: 'Doctor Vadim', date: '12.06.2017' },
    { procedure: 'Procedure 2 (B)', name: 'Profesof Maxim', date: '13.06.2017' },
    { procedure: 'Procedure 3 (C)', name: 'nurse Olga', date: '14.06.2017' },
    { procedure: 'Procedure 4 (D)', name: 'Docent Oleg', date: '15.06.2017' },
    { procedure: 'Procedure 5 (E)', name: 'Doctor Nicolai', date: '16.06.2017' } ];
// all dates must be in  Date object, but because this is test i use String, it must be done  in future.

function getDoctors(){
    // Because this is test only sample, I DONT USE DATABASE.
    // send request to database. because we haven't database right now, we will send data from predefined json obj
    return json;
}
function searchDoctorByDateAndProcedre(doctor,procedure,date){
    // Because this is test only sample, I DONT USE DATABASE.
    // send request to database. because we haven't database right now, we will send data from predefined json obj
   // database filtering imitation
    for (var i in json)
        if(json[i].name == doctor && json[i].procedure == procedure && json[i].date == date)return true;
    return false;

}

app.get('/' ,function (req,res) {
    res.sendFile(path.join(__dirname + '/admin.html'));
});
app.get('/doctors' ,function (req,res) {
    res.send(getDoctors());
});
app.get('/patient' , function (req,res) {
    db.collection('doctors').find().toArray(function (err  , docs) {
        if (err) {
            console.log(err);
            res.sendStatus(500)
        }
        res.send(docs);

    })
});
app.get('/patient/:id' , function (req,res) {
    app.use(bodyParser.json());
            db.collection('doctors').findOne({_id : ObjectID(req.params.id)} , function (err, doc) {
                if (err) {
                    console.log(err);
                   return res.sendStatus(500);
                }
                res.send(doc);
                res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
                res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            })
});
app.post('/searchDoctor' ,function (req,res) {
    var jsonData = {
        name : req.body.name,
        surname : req.body.surname,
        number : Number(req.body.number),
        mail : req.body.mail,
        date : req.body.date,
        doctor : req.body.doctor,
        procedure : req.body.procedure

    };
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        if ( searchDoctorByDateAndProcedre(jsonData.doctor,jsonData.procedure,jsonData.date ))  {
            db.collection('doctors').insert(jsonData , function (err , result) {
                res.sendStatus(200);
            });
        }
        else{
            res.sendStatus(500);
        }
});
//Delete request work's only in POSTMAN , because in client side it makes no sense
app.delete('/patient/:id' , function (req,res) {
         db.collection('doctors').deleteOne(
             {_id : ObjectID(req.params.id)},
             function (err, result) {
                 if (err){
                     console.log(err)
                     return res.sendStatus(500)
                 }
                 res.sendStatus(200)
             }
         )
});
MongoClient.connect('mongodb://localhost:27017/api' , function (err ,  database) {
    if (err) {
        return console.log(err);
    }
    db = database;
    app.listen(3334 , function () {
        console.log('Api working on 3334')
    })
});