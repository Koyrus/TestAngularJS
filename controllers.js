var app = angular.module('myApp',  ['720kb.datepicker']);
app.controller('myCtrl', function($scope, $http) {
    $http({
        method : "GET",
        url : "http://localhost:3334/patient"
    }).then(function mySucces(data) {
        $scope.items = angular.fromJson(data);
        console.info( $scope.items);
    });
});
app.controller('myJsonctrl', function($scope, $http ,$window) {
    $http({
        method : "GET",
        url : "http://localhost:3334/doctors"
    }).then(function mySucces(data) {
        $scope.json = data;
        console.info( $scope.json);
        console.info( JSON.stringify($scope.json));
    });

    $scope.login = function () {
        if ( $scope.doctor == undefined || $scope.date == undefined || $scope.procedure == undefined ){
            alert('net')
        }
        else{
            console.error($scope.doctor.procedure);
            function transformRequestToUrlEncoded(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            }
            $http({
                method: "POST",
                url : "http://localhost:3334/searchDoctor" ,
                data: {
                    name: $scope.name,
                    surname: $scope.lastname,
                    number: $scope.phone,
                    mail : $scope.mail,
                    date : new Date($scope.date).toLocaleDateString('ru-RU') ,
                    doctor : $scope.doctor.name,
                    procedure : $scope.doctor.procedure
                },headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                transformRequest: transformRequestToUrlEncoded
            })
                .success(function mySuccess (data) {
                    alert ("Success")
                    $window.location.reload()
                    console.log(data)
                })
                .error (function Error (data) {
                    alert("Sorry, procedure is not available in this day. Procedure is available it this day : "   +  $scope.doctor.date);
                    console.log('error');
                    console.log("-------------" , $scope.doctor.date)
                });
        }
    }
});

